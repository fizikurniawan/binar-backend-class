# Resources

>  Programming for everyone, happy hacking! :fire: :sunglasses: :fire:  

## Git :
*  [Git Introduction](https://git-scm.com/book/id/v1/Memulai-Git-Tentang-Version-Control)
*  [Git Book](https://books.goalkicker.com/GitBook/)
*  [Git Cheat Sheet](https://services.github.com/on-demand/downloads/id/github-git-cheat-sheet/)
*  [Git guide](http://rogerdudler.github.io/git-guide/index.id.html)

## Terminal and CodeEditor Cheat Sheet :
*  [Basic Linux Command](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners)
*  [VIM Cheat Sheet](https://gist.github.com/ervinismu/dc438d3668dbacb04ab36c65c4fb5570)

## Data Structure and Algorithm :
*  [Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/)
*  [Software Arcitecture](https://sourcemaking.com/)
*  [S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)

## Database
*  [Database Normalization](https://www.studytonight.com/dbms/database-normalization.php)
*  [Book Postgresql](https://books.goalkicker.com/PostgreSQLBook/)
*  [Setup Postgresql](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

## CI/CD
*  [.gitlab-ci.yml examples](https://docs.gitlab.com/ee/ci/examples/)
*  [.gitlab-ci Variables](https://docs.gitlab.com/ee/ci/variables/)

## Programming Language :
*  [Javascript](https://gitlab.com/ervinismu/binar-backend-class/blob/master/javascript.md)
*  [Ruby](https://gitlab.com/ervinismu/binar-backend-class/blob/master/ruby.md)
*  [Golang](https://gitlab.com/ervinismu/binar-backend-class/blob/master/golang.md)

## Interview :

* [Top 10 algorithms in interview question](https://www.geeksforgeeks.org/top-10-algorithms-in-interview-questions/)