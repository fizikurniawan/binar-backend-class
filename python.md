>  Resources for learning lang and framework python

### Python
-   [Python Web Scrapping (tutorialspoint)](https://www.tutorialspoint.com/python_web_scraping)
-   

### Framework
-   [CRUD Web App with Flask](https://scotch.io/tutorials/build-a-crud-web-app-with-python-and-flask-part-one)
-   [Restful api with python flask framework.](https://www.codementor.io/olawalealadeusi896/restful-api-with-python-flask-framework-and-postgres-db-part-1-kbrwbygx5)
-   [Flask Tutorial (tutorialspoint)](https://www.tutorialspoint.com/flask/index.htm)